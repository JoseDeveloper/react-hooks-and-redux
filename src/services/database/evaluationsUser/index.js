import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import { setAuthUser, updateLoadUser } from '../../../redux/actions/Auth';
import React from 'react';
import axios from '../../config';

export const evaluationsUser = {
  getEvaluationsByEvaluatorUser: (userId) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/getEvaluationsByEvaluatorUser', {
      user_id: userId
    })
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED => ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
  getEvaluationsByEvaluatedUser: (userId) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/getEvaluationsByEvaluatedUser', {
      user_id: userId
    })
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED => ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
  assignEvaluationToUser: (userId, userIdAssign, evaluationId, mode, evaluationUserId) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/assignEvaluationToUser', {
      user_id: userId,
      user_id_assign: userIdAssign,
      evaluation_id: evaluationId,
      mode,
      id: evaluationUserId
    })
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED => ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
  sendQualificationToUser: (id, qualification, userId) => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/sendQualificationToUser', {
      evaluation_user_id: id,
      qualification,
      user_id: userId
    })
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED => ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  saveUserResponseEvaluation: (userId, data) => {
    const { questions, id } = data; console.log('QUESIONS ',questions)
    const objectToSave = {
      user_id: userId,
      questions,
      id
    };

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;

    const response = axios.post('evaluation/saveUserResponseEvaluation', objectToSave)
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED response user => ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  }

};

export const evaluations = {
  getEvaluations: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get('evaluation/getEvaluations', {})
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED evaluations=> ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  saveDataEvaluation: (evaluationId, data, evaluationName) => {

    const objectToSave = {
      evaluation_name: evaluationName,
      evaluation_id: evaluationId,
      questions_and_answers: data
    };

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/saveEvaluation', objectToSave)
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED SAVE=> ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  }

};
