import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import { setAuthUser, updateLoadUser } from '../../../redux/actions/Auth';
import React from 'react';
import axios from '../../config';

export const evaluationsService = {
  getAllEvaluationsAndUsers: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('evaluation/getAllEvaluationsAndUsers', {})
      .then(({ data }) => {
        if (data) {
          console.log('DATA RESPONSED ALL AND USERSSS=> ', data)
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },
};
