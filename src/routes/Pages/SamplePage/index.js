import React from 'react';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Evaluations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});

function createData(date, title, score) {
  return { date, title, score };
}

const rows = [
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5'),
  createData("10-03-1989", 'Behavior evaluation', '5')
];



const Evaluations = () => {
  const classes = useStyles();

  const handleEdit = (e, row) => {
    e.preventDefault();
    console.log("The Values that you wish to edit ", row);
  };


  return (
    <PageContainer heading={<IntlMessages id="pages.evaluations" />} breadcrumbs={breadcrumbs}>
      <GridContainer>
        <Grid item xs={12}>
          <Box>
            <IntlMessages id="pages.evaluations.description" />
          </Box>
        </Grid>
        <TableContainer component={Paper}>
          <Table className={classes.table} aria-label="simple table">
            <TableHead>
              <TableRow>
                <TableCell>Date</TableCell>
                <TableCell align="right">Title</TableCell>
                <TableCell align="right">Score</TableCell>
                <TableCell align="right">Actions</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {rows.map(row => (
                <TableRow key={row.date}>
                  <TableCell component="th" scope="row">
                    {row.date}
                  </TableCell>
                  <TableCell align="right">{row.title}</TableCell>
                  <TableCell align="right">{row.score}</TableCell>
                  <TableCell align="right">
                    <a href="#" onClick={(e) => handleEdit(e, row)}><VisibilityIcon /></a>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
      </GridContainer>
    </PageContainer>
  );
};

export default Evaluations;
