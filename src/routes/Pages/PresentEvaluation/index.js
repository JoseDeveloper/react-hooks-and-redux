import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import VisibilityIcon from '@material-ui/icons/Visibility';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import DoneAllIcon from '@material-ui/icons/DoneAll';

// Dialog.
import FormDialog from "./customComponents/modal";

// Services.
import { evaluationsUser } from '../../../../src/services/database/evaluationsUser';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Evaluations', isActive: true },
];

const useStyles = makeStyles({
  table: {
    minWidth: 650
  }
});


const PresentEvaluations = () => {
  const classes = useStyles();
  const { authUser } = useSelector(({ auth }) => auth);

  const [evaluationsData, setEvaluations] = useState([]);
  const [openDialog, setOpenDialog] = useState(false);
  const [dialogData, setDialogData] = useState({});

  useEffect(() => {

    async function getEvaluationsUser() {
      const { id: userId } = authUser;
      const response = await evaluationsUser.getEvaluationsByEvaluatedUser(userId);
      const { data } = response;
      setEvaluations(data);
    }

    getEvaluationsUser();
  }, []);

  const handleOpenDialog = (e, row) => {
    e.preventDefault();
    setOpenDialog(true);
    setDialogData(row);
  };

  const handleCloseDialog = () => {
    setOpenDialog(false);
    setDialogData({});
  };

  const actualizeData = (data) => {
    setEvaluations(data);
    setOpenDialog(false);
    handleCloseDialog();
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.presentEvaluation" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.presentEvaluation.description" />
            </Box>
          </Grid>
          <TableContainer component={Paper}>
            <Table className={classes.table} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Date</TableCell>
                  <TableCell align="right">Title</TableCell>
                  <TableCell align="right">Score</TableCell>
                  <TableCell align="right">Presented</TableCell>
                  <TableCell align="right">User score</TableCell>
                  <TableCell align="right">Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {evaluationsData.map(row => (
                  <TableRow key={row.evaluationDate}>
                    <TableCell component="th" scope="row">
                      {row.evaluationDate}
                    </TableCell>
                    <TableCell align="right">{row.evaluationName}</TableCell>
                    <TableCell align="right">{row.evaluationScore}</TableCell>
                    <TableCell align="right">{row.presented}</TableCell>
                    <TableCell align="right">{row.userScore}</TableCell>
                    {row.presented != 1 ?
                      <TableCell align="right">
                        <a href="#" onClick={(e) => handleOpenDialog(e, row)}><VisibilityIcon /></a>
                      </TableCell>
                      : <TableCell align="right">
                        <DoneAllIcon />
                      </TableCell>
                    }

                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </GridContainer>

      </PageContainer>
      <FormDialog
        openDialog={openDialog}
        dialogData={dialogData}
        closeDialog={handleCloseDialog}
        actualizeData={actualizeData}
      />
    </>
  );
};

export default PresentEvaluations;
