import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import RadioButtonsGroup from "./radioGroups";
import QuestionsAddedText from "./questionTextAdded";

// Services.
import { evaluationsUser } from '../../../../../src/services/database/evaluationsUser';

export default function FormDialog({ openDialog, dialogData, closeDialog, actualizeData }) {
    const { authUser } = useSelector(({ auth }) => auth);
    const [open, setOpen] = React.useState(false);
    const [data, setData] = React.useState(false);

    useEffect(() => {
        setOpen(openDialog);
        setData(dialogData);
    }, [openDialog, dialogData]);

    const handleClose = () => {
        setOpen(false);
        closeDialog();
    };

    const setUserResponses = (n, value) => {
        let tempData = data;
        let selectedTempData = tempData.questions[n];
        let answers = selectedTempData.answers_decode;
        answers.map((data, i) => {
            if (data.option === value) {
                answers[i].userResponse = true;
            } else {
                answers[i].userResponse = false;
            }
        })

        selectedTempData.answers_decode = answers;
        tempData.questions[n] = selectedTempData;
        setData(tempData);
        
    }

    const setUserResponsesText = (n, value) => {
        let tempData = data;
        let selectedTempData = tempData.questions[n];
        let answers = selectedTempData.answers_decode;
        answers = { 'userResponse' : value };
        selectedTempData.answers_decode = answers;
        tempData.questions[n] = selectedTempData;
        
        setData(tempData);

    }

    const sendResponse = async () => { 
        const { id: userId } = authUser;
        const response = await evaluationsUser.saveUserResponseEvaluation(userId, data); 
        const { data:responseData } = response;
        actualizeData(responseData);
    }

    return (
        <div>
            <Dialog
                open={open}
                onClose={handleClose}
                fullWidth={true}
                maxWidth={'sm'}
                aria-labelledby="form-dialog-title"
            >
                <DialogTitle id="form-dialog-title">Questions and answers</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        {data.evaluationName}
                    </DialogContentText>
                    {
                        data.questions && data.questions.map((item, i) => { console.log('ITEM ',item)
                            if(item.type === 1){
                                return (<RadioButtonsGroup key= {i} n={i} questions={item} setUserResponses={setUserResponses} />)
                            }else{
                                return (<QuestionsAddedText key= {i} n={i} questions={item} setUserResponses={setUserResponsesText} />)
                            }

                        })
                    }

                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Cancel
          </Button>
                    <Button onClick={sendResponse} color="primary">
                        Send
          </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}