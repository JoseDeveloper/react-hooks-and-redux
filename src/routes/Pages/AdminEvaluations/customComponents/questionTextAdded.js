import React, { useEffect, forwardRef, useImperativeHandle } from 'react';
import { makeStyles } from "@material-ui/core/styles";
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import CheckIcon from '@material-ui/icons/Check';
import DoneAllIcon from '@material-ui/icons/DoneAll';
import EditIcon from '@material-ui/icons/Edit';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import TextField from '@material-ui/core/TextField';

const useStyles = makeStyles({
    fieldText: {
        width: '100%'
    },
    areaA: {
        marginLeft: '30px'
    },
    addOption: {
        height: '30px'
    },
});

const QuestionsAddedText = (props, ref) => {

    const classes = useStyles();
    const [textResponse, setText] = React.useState('');
    const [question, setQuestion] = React.useState('');

    useEffect(() => {
        setQuestion(props.questions.question);

    }, [props.questions]);

    const handleChange = (event) => {
        const { value } = event.target;
        setText(value);
        const { n, setUserResponses } = props;
        setUserResponses(n, value);
    };

    return (
        <FormControl component="fieldset">
            <span>{question}</span>
            {props.questions.user_answers &&
                <span>{props.questions.user_answers.user_answers.userResponse}</span>
            }

        </FormControl >
    )

}

export default React.forwardRef(QuestionsAddedText);